package com.chat.ChatMicroservice.UserMicroservice.Configuration;

import com.chat.ChatMicroservice.UserMicroservice.Service.Mapper.UserMapper;
import com.chat.ChatMicroservice.UserMicroservice.Service.Mapper.UserMapperImplements;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class BeanConfiguration {

    @Bean
    public UserMapper userMapper(PasswordEncoder passwordEncoder){
        return new UserMapperImplements(passwordEncoder);
    }
}

package com.chat.ChatMicroservice.UserMicroservice.Service.Mapper;

import com.chat.ChatMicroservice.UserMicroservice.DTO.UserDTO;
import com.chat.ChatMicroservice.UserMicroservice.DTO.UserRequestDTO;
import com.chat.ChatMicroservice.UserMicroservice.Entity.UserEntity;

public interface UserMapper {
    public UserDTO map(UserEntity userEntity);
    public UserEntity map(UserRequestDTO userRequestDTO) throws Exception;
    public UserEntity map(UserDTO userDTO);
}

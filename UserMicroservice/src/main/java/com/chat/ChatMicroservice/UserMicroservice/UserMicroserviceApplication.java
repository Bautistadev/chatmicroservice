package com.chat.ChatMicroservice.UserMicroservice;

import com.chat.ChatMicroservice.UserMicroservice.Entity.UserEntity;
import com.chat.ChatMicroservice.UserMicroservice.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.NoSuchElementException;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.chat.ChatMicroservice"})
public class UserMicroserviceApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(UserMicroserviceApplication.class, args);
	}

	@Value("${spring.default.username}")
	private String default_username;
	@Value("${spring.default.password}")
	private String defautl_password;

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public void run(String... args) throws Exception {
		UserRepository userRepository = this.applicationContext.getBean(UserRepository.class);
		try {
			userRepository.findByUsername(default_username).get();
		}catch (NoSuchElementException e){
			UserEntity userEntity = UserEntity.builder()
					.dni(0000000000000)
					.name(default_username)
					.username(default_username)
					.email("batubasiliodev@outlook.com")
					.password(this.passwordEncoder.encode(this.defautl_password))
					.lastname(default_username)
					.build();
			userRepository.save(userEntity);
		}

	}
}

package com.chat.ChatMicroservice.UserMicroservice.Service.Mapper;

import com.chat.ChatMicroservice.UserMicroservice.DTO.UserDTO;
import com.chat.ChatMicroservice.UserMicroservice.DTO.UserRequestDTO;
import com.chat.ChatMicroservice.UserMicroservice.Entity.UserEntity;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserMapperImplements implements UserMapper{

    private PasswordEncoder passwordEncoder;

    public UserMapperImplements(PasswordEncoder passwordEncoder){
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDTO map(UserEntity userEntity) {

        UserDTO userDTO = UserDTO.builder()
                .id(userEntity.getId())
                .name(userEntity.getName())
                .username(userEntity.getUsername())
                .password(userEntity.getPassword())
                .dni(userEntity.getDni())
                .email(userEntity.getEmail())
                .lastname(userEntity.getLastname())
                .build();

        return userDTO;
    }

    @Override
    public UserEntity map(UserRequestDTO userRequestDTO) throws Exception {
        if(userRequestDTO == null)
            throw new NullPointerException("Error : UserMapperImplements : UserRequestDTO -> UserEntity : data not by null ");

        if(userRequestDTO.getDni().toString().length() < 11)
            throw new IllegalArgumentException("Error : UserMapperImplements : UserRequestDTO -> UserEntity : dni is lower 11 character ");

        UserEntity userEntity = UserEntity.builder()
                .name(userRequestDTO.getName())
                .username(userRequestDTO.getUsername())
                .password(this.passwordEncoder.encode(userRequestDTO.getPassword()))
                .dni(userRequestDTO.getDni())
                .email(userRequestDTO.getEmail())
                .lastname(userRequestDTO.getLastname())
                .build();

        return userEntity;
    }

    @Override
    public UserEntity map(UserDTO userDTO) {
        if(userDTO == null)
            throw new NullPointerException("Error : UserMapperImplements : UserRequestDTO -> UserEntity : data not by null ");

        if(userDTO.getDni().toString().length() < 11)
            throw new IllegalArgumentException("Error : UserMapperImplements : UserRequestDTO -> UserEntity : dni is lower 11 character ");


        UserEntity userEntity = UserEntity.builder()
                .id(userDTO.getId())
                .name(userDTO.getName())
                .username(userDTO.getUsername())
                .password(userDTO.getPassword())
                .dni(userDTO.getDni())
                .email(userDTO.getEmail())
                .lastname(userDTO.getLastname())
                .build();
        return null;
    }
}

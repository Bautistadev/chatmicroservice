package com.chat.ChatMicroservice.UserMicroservice.Repository;

import com.chat.ChatMicroservice.UserMicroservice.Entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity,Integer> {

    public Optional<UserEntity> findByUsername(String username);
}

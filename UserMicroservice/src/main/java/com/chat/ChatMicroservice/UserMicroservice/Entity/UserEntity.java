package com.chat.ChatMicroservice.UserMicroservice.Entity;

import jakarta.persistence.*;
import lombok.*;

import javax.validation.constraints.Size;

@Entity
@Table(name = "User")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "username",nullable = false,unique = true)
    private String username;

    @Column(name = "password",nullable = false)
    private String password;

    @Column(name = "name",nullable = false,unique = false)
    private String name;

    @Column(name = "lastname",nullable = false,unique = false)
    private String lastname;

    @Column(name = "email",nullable = false,unique = true)
    private String email;

    @Column(name = "dni",nullable = false,unique = true)
    @Size(min = 11)
    private Integer dni;


}
